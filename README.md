# 28- Find the Index of the First Occurrence in a String

https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/description/?envType=featured-list&envId=top-interview-questions?envType=featured-list&envId=top-interview-questions

- So the problem is if the string in ***needle*** appears in haystack
- return the index it happens at, otherwise returnn -1